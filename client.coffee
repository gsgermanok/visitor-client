# ##############################
# this is a client in JavaScript for get the information of online users
# author: ggerman@gmail.com
# date: 30 Oct 2015
# code: CoffeeScript
# compile with: coffee --compile client.coffee
#

out = { 
  version: "0.1a"
  author: {
    name: "Gimenez Silva German Alberto"
    email: "ggerman@gmail.com"
  }

  ip:"0.0.0.0" 

  battery: { 
    charging: 0
    level: 0
    chargingTime: 0
    dischargingTime: 0 
  }

  geo: {
    latitude: 0
    longitude: 0
    accuracy: 0
    altitude: 0
    altitudeAccuracy: 0
    heading: 0
    speed: 0 
  }

  appCodeName: ""
  appName: ""
  appName: ""
  appVersion: ""
  cookieEnabled: ""
  onLine: ""
  platform: ""
  product: ""
  userAgent: ""
  maxTouchPoints: "empty"
  url: ""
  referrer: ""
  }

(($) ->

) jQuery

### ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# this block of code if for repeat process while the user is online
###

count = 0
setInterval( ->

  ### ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  # recovery ip of visitor and information from 
  # browser
  # cell phone
  # geo position
  # battery
  # movement
  # vibration
  ### 
  #
  $.getJSON("http://127.0.0.1:3000/widgets/save?info=" + dump(), (info) ->
  )
  $.getJSON("http://jsonip.com?callback=?", (data) ->
    out.ip = data.ip
    info()
  )
  null

, 1000)

info = ->
  # #######################################################
  # Navigator { mozPay: null, mozContacts: ContactManager, mozApps: DOMApplicationsRegistry, mozTCPSocket: null, mimeTypes: MimeTypeArray, plugins: PluginArray, doNotTrack: "unspecified", oscpu: "Linux x86_64", vendor: "", vendorSub: "" }
  #

  out.url = document.location
  out.referrer = document.referrer

  nav = navigator
  for keys, values of nav
    switch keys
      when "appCodeName" then out.appCodeName = nav.appCodeName
      when "geolocation" then nav.geolocation.getCurrentPosition(showPosition)
      when "appName"  then out.appName = nav.appName
      when "appVersion" then out.appVersion = nav.appVersion
      when "cookieEnabled" then out.cookieEnabled = nav.cookieEnabled
      when "language" then out.language = nav.language
      when "languages" then out.languages = nav.languages.toString().replace(/([;,])/, "|")
      when "onLine" then out.onLine = nav.onLine
      when "platform" then out.platform = nav.platform
      when "product" then out.product = nav.product
      when "maxTouchPoints" then out.maxTouchPoints = nav.maxTouchPoints
      when "userAgent" then out.userAgent = nav.userAgent # .replace(/([;,])/, 'Y')
      when "taintEnabled" then  "~~> " + nav.taintEnabled()
      when "battery" then battery(nav)
      else # keys + "~> " + values



battery = (nav) ->      
  out.battery.charging = nav.battery.charging
  out.battery.level = nav.battery.level
  out.battery.chargingTime = nav.battery.chargingTime
  out.battery.dischargingTime = nav.battery.dischargingTime

# ##############################################
# Write in the console using the l() function
#
l = (msg) ->
  console.log msg

getStatus = (reg) ->
  l reg.length
  l reg.name

showPosition = (pos) ->
 out.geo.latitude = pos.coords.latitude
 out.geo.longitude = pos.coords.longitude
 out.geo.accuracy = pos.coords.accuracy
 out.geo.altitude = pos.coords.altitude
 out.geo.altitudeAccuracy = pos.coords.altitudeAccuracy
 out.geo.heading = pos.coords.heading
 out.geo.speed = pos.coords.speed 

dump = ->
  string = JSON.stringify out
  string.replace(/([\;\&])/g, '|')


